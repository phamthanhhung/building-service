﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Common.Middleware
{
    public class ErrorLoggingMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorLoggingMiddleware"/> class
        /// </summary>
        /// <param name="next"></param>
        public ErrorLoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
