﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Infrastructure.DTO
{
    public class GeoJsonModel
    {
        public string Type { get; set; } = "FeatureCollection";
        public List<FeatureModel> Features {get;set;}
    }
}
