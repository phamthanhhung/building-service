﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class Pointlayer
    {
        public int Gid { get; set; }
        public long? Objectid { get; set; }
        public string OsmId { get; set; }
        public int? Code { get; set; }
        public string Fclass { get; set; }
        public string Name { get; set; }
        public string Phanloai { get; set; }
        public Point Geom { get; set; }
        public double? MinDistance { get; set; }
        public int? Cid { get; set; }
        public int? Ordered { get; set; }
    }
}
