﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Apikey> Apikey { get; set; }
        public virtual DbSet<ClusterTb> ClusterTb { get; set; }
        public virtual DbSet<Khoinhahn> Khoinhahn { get; set; }
        public virtual DbSet<Pointconfig> Pointconfig { get; set; }
        public virtual DbSet<Pointlayer> Pointlayer { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Host=10.159.21.212;Port=5432;Database=t-server;Username=postgres;Password=admin@123", x => x.UseNetTopologySuite());
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("postgis");

            modelBuilder.Entity<Apikey>(entity =>
            {
                entity.ToTable("apikey");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Expriry)
                    .HasColumnName("expriry")
                    .HasColumnType("date");

                entity.Property(e => e.Key)
                    .HasColumnName("key")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<ClusterTb>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cluster_tb");

                entity.Property(e => e.Cid).HasColumnName("cid");

                entity.Property(e => e.Gid).HasColumnName("gid");
            });

            modelBuilder.Entity<Khoinhahn>(entity =>
            {
                entity.HasKey(e => e.Gid)
                    .HasName("khoinhahn_pkey");

                entity.ToTable("khoinhahn");

                entity.HasIndex(e => e.Geom)
                    .HasName("khoinhahn_geom_idx")
                    .HasMethod("gist");

                entity.Property(e => e.Gid).HasColumnName("gid");

                entity.Property(e => e.Chieucao)
                    .HasColumnName("chieucao")
                    .HasColumnType("numeric");

                entity.Property(e => e.Doituong).HasColumnName("doituong");

                entity.Property(e => e.Geom)
                    .HasColumnName("geom")
                    .HasColumnType("geometry(MultiPolygon)");

                entity.Property(e => e.HightInch)
                    .HasColumnName("hight_inch")
                    .HasColumnType("numeric");

                entity.Property(e => e.Madoituong)
                    .HasColumnName("madoituong")
                    .HasMaxLength(4);

                entity.Property(e => e.Manhandang)
                    .HasColumnName("manhandang")
                    .HasMaxLength(18);

                entity.Property(e => e.Ngaycapnha)
                    .HasColumnName("ngaycapnha")
                    .HasColumnType("date");

                entity.Property(e => e.Ngaythunha)
                    .HasColumnName("ngaythunha")
                    .HasColumnType("date");

                entity.Property(e => e.Objectid).HasColumnName("objectid");

                entity.Property(e => e.Objectid1).HasColumnName("objectid_1");

                entity.Property(e => e.ShapeArea)
                    .HasColumnName("shape_area")
                    .HasColumnType("numeric");

                entity.Property(e => e.ShapeLe1)
                    .HasColumnName("shape_le_1")
                    .HasColumnType("numeric");

                entity.Property(e => e.ShapeLeng)
                    .HasColumnName("shape_leng")
                    .HasColumnType("numeric");

                entity.Property(e => e.Ten)
                    .HasColumnName("ten")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Pointconfig>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("pointconfig");

                entity.Property(e => e.Level).HasColumnName("level");

                entity.Property(e => e.Ordered)
                    .HasColumnName("ordered")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<Pointlayer>(entity =>
            {
                entity.HasKey(e => e.Gid)
                    .HasName("gis_osm_pois_free_1_all_pkey");

                entity.ToTable("pointlayer");

                entity.HasIndex(e => e.Geom)
                    .HasName("gis_osm_pois_free_1_all_geom_idx")
                    .HasMethod("gist");

                entity.HasIndex(e => e.Gid)
                    .HasName("gid_index");

                entity.Property(e => e.Gid)
                    .HasColumnName("gid")
                    .HasDefaultValueSql("nextval('gis_osm_pois_free_1_all_gid_seq'::regclass)");

                entity.Property(e => e.Cid).HasColumnName("cid");

                entity.Property(e => e.Code).HasColumnName("code");

                entity.Property(e => e.Fclass)
                    .HasColumnName("fclass")
                    .HasMaxLength(28);

                entity.Property(e => e.Geom)
                    .HasColumnName("geom")
                    .HasColumnType("geometry(Point)");

                entity.Property(e => e.MinDistance).HasColumnName("min_distance");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(100);

                entity.Property(e => e.Objectid).HasColumnName("objectid");

                entity.Property(e => e.Ordered).HasColumnName("ordered");

                entity.Property(e => e.OsmId)
                    .HasColumnName("osm_id")
                    .HasMaxLength(10);

                entity.Property(e => e.Phanloai)
                    .HasColumnName("phanloai")
                    .HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
