﻿using System;
using System.Collections.Generic;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class Map
    {
        public int? ZoomLevel { get; set; }
        public int? TileColumn { get; set; }
        public int? TileRow { get; set; }
        public string TileId { get; set; }
        public string GridId { get; set; }
    }
}
