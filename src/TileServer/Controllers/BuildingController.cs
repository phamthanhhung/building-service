﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SI.GIS.BuildingServer.Common;
using SI.GIS.BuildingServer.Services.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Controllers
{   
    [ApiController]
    [Route("building")]
    public class BuildingController : ControllerBase
    {
        private readonly IBuildingService _buildingService;

        public BuildingController(IBuildingService buildingService)
        {
            _buildingService = buildingService;
        }

        /// <summary>
        /// Hàm get building hiển thị trên base map sử dụng lib osmbuilding
        /// </summary>
        /// <param name="z"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoomlv"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("basemap/{z}/{x}/{y}")]
        public async Task<IActionResult> GetTileJsonBasemap([FromRoute(Name = "z")] int z, [FromRoute(Name = "x")] int x, [FromRoute(Name = "y")] int y, [FromQuery] int zoomlv)
        {

            Response.Headers.Add("cache-control", "max-age=86400");

            var SW_long = Utils.Tile2long(x, z).ToString();

            var SW_lat = Utils.Tile2lat(y + 1, z).ToString();

            var NE_long = Utils.Tile2long(x + 1, z).ToString();

            var NE_lat = Utils.Tile2lat(y, z).ToString();

            var bbox = SW_long + ',' + SW_lat + ',' + NE_long + ',' + NE_lat;

            var data = await _buildingService.GetBuildingForBaseMap(bbox, z);

            return Ok(data);

        }

        /// <summary>
        /// Hàm get building hiển thị trên 3D map sử dụng lib osmbuilding
        /// </summary>
        /// <param name="z"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoomlv"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{z}/{x}/{y}")]
        public async Task<IActionResult> GetTileJson([FromRoute(Name = "z")] int z, [FromRoute(Name = "x")] int x, [FromRoute(Name = "y")] int y)
        {

            Response.Headers.Add("cache-control", "max-age=86400");

            var SW_long = Utils.Tile2long(x, z).ToString();

            var SW_lat = Utils.Tile2lat(y + 1, z).ToString();

            var NE_long = Utils.Tile2long(x + 1, z).ToString();

            var NE_lat = Utils.Tile2lat(y, z).ToString();

            var bbox = SW_long + ',' + SW_lat + ',' + NE_long + ',' + NE_lat;

            var data = await _buildingService.GetBuilding3D(bbox, z);

            return Ok(data);

        }

        [HttpGet]
        [Route("tms/{z}/{x}/{y}.pbf")]
        public async Task<IActionResult> GetTileBuildingLayer([FromRoute(Name = "z")] int z, [FromRoute(Name = "x")] int x, [FromRoute(Name = "y")] int y, [FromQuery] string geoCol, [FromQuery] string layername)
        {
            Response.Headers.Add("cache-control", "max-age=86400");
            var bbox = Utils.GetBBoxFromTile(z, x, y);
            var data = await _buildingService.GetTileBuilding(bbox);

            if (data == null)
                return NoContent();
            else
                return File(data, "application/x-protobuf");
        }
        /* CÁC API ĐANG TEST VỚI CESIUMJS */

        [HttpGet]
        [Route("3d-tiles")]
        public IActionResult Get3DTiles()
        {

            var listTileset = new List<string>();
            for (var folderId = 108048; folderId < 119900; folderId++)
            {
                string tilesetUrl = "http://localhost:9000/building/" + folderId.ToString() + "/tileset.json";
                listTileset.Add(tilesetUrl);
            }

            return Ok(listTileset); 

        }

        [HttpGet]
        [Route("building/{id}/tileset.json")] 
        public IActionResult GetTilesetJson([FromRoute(Name = "id")] string id)
        {
            string folderPath = "C:/CODE/building_b3dm/";
            folderPath = folderPath + id + "/tileset.json";

            if (System.IO.File.Exists(folderPath))
            {
                var tileset = PhysicalFile(folderPath, "application/json");

                return tileset;
            }

            return NoContent();
        }

        [HttpGet]
        [Route("building/{id}/data/data0.b3dm")]
        public IActionResult GetTilesetB3dm([FromRoute(Name = "id")] string id)
        {
            string folderPath = "C:/CODE/building_b3dm/";
            folderPath = folderPath + id + "/data/data0.b3dm";

            if (System.IO.File.Exists(folderPath))
            {
                var tileset = PhysicalFile(folderPath, "application/octet-stream");

                return tileset;
            }

            return NoContent();
        }
    }
}
