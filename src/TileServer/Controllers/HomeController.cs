﻿using Microsoft.AspNetCore.Mvc;

namespace SI.GIS.BuildingServer.Controllers
{
    /// <summary>
    /// HomeController
    /// </summary>
    [Route("")]
    public class HomeController : ControllerBase
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("VNPT SI.GIS.BuildingServer");
        }

        /// <summary>
        /// Ping
        /// </summary>
        /// <returns></returns>
        [HttpGet("ping")]
        public IActionResult Ping() => Ok();
    }
}
